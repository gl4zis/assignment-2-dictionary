%define TERMINATOR 0
%define EXIT_CODE 60
%define WRITE_CODE 1
%define READ_CODE 0
%define STDERR 2
%define STDOUT 1
%define STDIN 0
%define NEWLINE `\n`
%define ASCII_ZERO '0'
%define SPACE ' '
%define TAB `\t`
%define ASCII_MINUS '-'
%define ASCII_PLUS '+'

section .data
    err db 'Error during reading stream'

section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global print_error
 
; Принимает код возврата и завершает текущий процесс
; Input: rdi - return code
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; Input: rdi - string
; Output: rax - length
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], TERMINATOR
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; Input: rdi - string
print_string:
    push rdi
    call string_length
    mov rdx, rax       ; string length in bytes
    pop rsi     ; string address
    mov rax, WRITE_CODE         ; 'write' syscall number
    mov rdi, STDOUT           ; stdout descriptor
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE

; Принимает код символа и выводит его в stdout
; Input: rdi - char
print_char:
    push rdi
    mov rsi, rsp     ; string address
    mov rax, WRITE_CODE           ; 'write' syscall number
    mov rdi, STDOUT           ; stdout descriptor
    mov rdx, 1        ; string length in bytes
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; Input: rdi - num
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, ASCII_MINUS
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Input: rdi - num
print_uint:
    push rbx
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 32
    mov rbx, 10
    mov byte [rdi], 0
    .loop:
        dec rdi
        xor rdx, rdx
        div rbx
        add rdx, ASCII_ZERO
        mov [rdi], dl
        test rax, rax
        jnz .loop
    .print:
        call print_string
        add rsp, 32
        pop rbx
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; Input: rdi, rsi - strings
; Output: rax - 1/0
string_equals:
    xor rcx, rcx
    .loop:
        mov al, byte [rdi+rcx]
        cmp al, byte [rsi+rcx]
        jne .false
        test al, al
        jz .true
        inc rcx
        jmp .loop
    .false:
        xor rax, rax
        ret
    .true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; Output: rax - 0/char
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp     ; string address
    mov rdi, STDIN           ; stdin descriptor
    mov rdx, 1        ; string length in bytes  
    syscall
    cmp rax, -1
    jg .ok
    cmp rax, -4095
    jl .ok
    mov rdi, err
    xor rax, rax
    jmp print_error
    .ok:
        pop rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; Input: rdi - buffer, rsi - buffer length
; Output: rax - buffer/0, rdx - length
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .space_loop:
        call read_char
        test al, al
        jz .ok
        cmp rax, SPACE
        je .space_loop
        cmp rax, TAB
        je .space_loop
        cmp rax, NEWLINE
        je .space_loop
    .loop:
        mov byte [r12+r14], al
        inc r14
        cmp r14, r13
        jg .fail
        call read_char
        test al, al
        jz .ok
        cmp rax, SPACE
        je .ok
        cmp rax, TAB
        je .ok
        cmp rax, NEWLINE
        je .ok
        jmp .loop
    .fail:
        xor rax, rax
        jmp .end
    .ok:
        mov byte [r12+r14], TERMINATOR
        mov rax, r12
        mov rdx, r14
    .end:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; Input: rdi - string
; Output: rax - num, rdx - num length/0
parse_uint:
    xor r8, r8
    xor rax, rax
    xor rcx, rcx
    mov rsi, 10
    .loop:
        mov cl, byte [rdi+r8]
        sub cl, ASCII_ZERO
        test cl, cl
        jl .ok
        cmp cl, 9
        jg .ok
        inc r8
        mul rsi
        add rax, rcx
        jmp .loop
    .ok:
        mov rdx, r8
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Input: rdi - string
; Output: rax - num, rdx - num length/0
parse_int:
    mov al, byte [rdi]
    cmp al, ASCII_MINUS
    je .neg
    cmp al, ASCII_PLUS
    jne parse_uint
    inc rdi
    jmp parse_uint

    .neg:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .fail 
        neg rax
        inc rdx
        ret
    .fail:
        xor rdx, rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Input: rdi - string, rsi - buffer, rdx - buffer length
; Output: rax - string_length/0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        jge .fail
        mov al, byte [rdi+rcx]
        mov byte [rsi+rcx], al
        inc rcx
        test al, al
        je .ok
        jmp .loop
    .fail:
        xor rax, rax
        ret
    .ok:
        mov rax, rcx
        ret

print_error:
    push rdi
    call string_length
    mov rdx, rax       ; string length in bytes
    pop rsi     ; string address
    mov rax, WRITE_CODE         ; 'write' syscall number
    mov rdi, STDERR           ; stdout descriptor
    syscall
    ret