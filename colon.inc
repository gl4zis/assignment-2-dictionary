%define dict 0
%macro colon 2
%ifid %2
    %ifstr %1
        %2:
            dq dict
            db %1, 0
        %define dict %2
    %else
        %error "Invalid key"
    %endif
%else
    %error "Invalid label"
%endif
%endmacro