%include "dict.inc"
%include "lib.inc"
%include "words.inc"

section .data
    read_err: db 'Fail during reading string', 0
    find_err: db 'There is no such element', 0

section .bss
    key: resb 256

section .text

global _start

_start:
    mov rdi, key
    mov rsi, 255
    call read_word
    test rax, rax
    je .read_fail
    get_value rax, rdx
    call print_string
    call print_newline
    jmp .end
    .find_fail:
        mov rdi, find_err
        jmp .print_err
    .read_fail:
        mov rdi, read_err
    .print_err:
        call print_error
        call print_newline
    .end:
        xor rdi, rdi
        call exit