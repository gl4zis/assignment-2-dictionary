asm = nasm
flags = -f elf64 -o

build: main.o
	ld -o build/main main.o colon.o lib.o words.o dict.o
	make clean

main.o: main.asm dict.o words.o
	$(asm) $(flags) main.o main.asm

lib.o: lib.asm
	$(asm) $(flags) lib.o lib.asm

dict.o: dict.asm lib.o
	$(asm) $(flags) dict.o dict.asm

words.o: words.inc colon.o 
	$(asm) $(flags) words.o words.inc

colon.o: colon.inc
	$(asm) $(flags) colon.o colon.inc

.PHONY: clean
clean:
	rm *.o

.PHONY: test
test:
	python3 test.py

.PHONY: run
run: build
	./build/main
