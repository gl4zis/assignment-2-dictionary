%include "lib.inc"

section .text

global find_word

; Input: rdi - string start, rsi - dictionary start
; Output: rax - address of word in dict | 0
find_word:
    push rbx
    push r12
    mov rbx, rsi
    mov r12, rdi
    .loop:
        mov rdi, r12
        mov rsi, rbx
        add rsi, 8
        call string_equals
        test rax, rax
        jne .ok
        mov rbx, [rbx]
        test rbx, rbx
        jne .loop
        xor rax, rax
        jmp .end
    .ok:
        mov rax, rbx
    .end:
        pop r12
        pop rbx
        ret