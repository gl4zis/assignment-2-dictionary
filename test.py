from subprocess import *
import os

tests = ["first", "fifth", "noItem", ''.join("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibu".split(" "))]
results = ["this is first item", "this is fifth item", "", ""]
errors = ["", "",  "There is no such element", "Fail during reading string"]
words_start = """%include "colon.inc"
section .data"""
dicts = [words_start + """
colon "first", first
db "this is first item", 0

colon "second", second
db "this is second item", 0

colon "fifth", fifth
db "this is fifth item", 0""",

words_start + """
colon first, first
db "this is first item", 0""",

words_start + """
colon "first", "first"
db "this is first item", 0"""]

codes = [0, 2, 2]


def rewrite_words(file: str) -> int:
    os.remove("words.inc")
    with open("words.inc", 'w') as f:
        f.writelines(file)
    return call(['make', 'build'], stdout=DEVNULL, stderr=DEVNULL)


def check_build_code(index: int) -> bool:
    code = rewrite_words(dicts[index])
    print("Build code:", code, end=' ')
    if code == codes[index]:
        print('OK')
    else:
        print('Must be ', codes[index], '!', sep='')
    return code == codes[index]


def main() -> None:
    all_tests_res = check_build_code(0)
    for i in range(len(tests)):
        test = tests[i]
        result = results[i]
        error = errors[i]
        program = Popen(['./build/main'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
        out, err = program.communicate(input=test.encode())

        out = out.decode().strip()
        err = err.decode().strip()

        test_res = (out == result and err == error)
        all_tests_res = all_tests_res and test_res

        if test_res:
            print('.', end='')
        else:
            print('F', end='')

    print()

    all_tests_res = all_tests_res and check_build_code(1)
    all_tests_res = all_tests_res and check_build_code(2)

    if all_tests_res:
        print('All tests was completed successfully')
    else:
        print('Some test was failed')

    call(["make", "clean"], stdout=DEVNULL, stderr=DEVNULL)
    check_build_code(0)


if __name__ == "__main__":
    main()