extern find_word
%define LABEL_LEN 8
%macro get_value 2  ; Args - key, key_length
    push %2
    mov rdi, %1
    mov rsi, dict
    call find_word
    test rax, rax
    je .find_fail
    mov rdi, rax
    add rdi, LABEL_LEN
    pop rdx
    add rdi, rdx
    inc rdi
%endmacro